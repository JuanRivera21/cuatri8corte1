llamadoFetch =()=>{
    const name = document.getElementById("nombre").value;
    const url = "https://restcountries.com/v3.1/name/"+name;
    fetch(url)
    .then(respuesta => respuesta.json())
    .then(data => mostrarTodos(data))
    .catch((reject)=>{
        console.log("Surgió un error "+ reject)
    })
}

const mostrarTodos=(data)=>{
    console.log(data)

    const cap = document.getElementById("capital");
    const len = document.getElementById("lenguaje");

    cap.value=data[0].capital;
    if (data[0].languages) {
        // Itera sobre las claves del objeto languages y toma el primer idioma que encuentre
        for (const key in data[0].languages) {
            if (data[0].languages.hasOwnProperty(key)) {
                len.value = data[0].languages[key];
                break; // Detiene la iteración después de encontrar el primer idioma
            }
        }
    } else {
        len.value = "Idioma no especificado";
    }

}

document.getElementById("btnBuscar").addEventListener('click', function(){
    llamadoFetch();
})

document.getElementById("btnLimpiar").addEventListener('click', function(){
    const cap = document.getElementById("capital");
    const len = document.getElementById("lenguaje");

    cap.value="";
    len.value="";
});