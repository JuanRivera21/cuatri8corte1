function hacerPeticion() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";


    http.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            const UsuariosBody = document.getElementById("UsuariosBody");
            const usuarios = JSON.parse(this.responseText);
            UsuariosBody.innerHTML = "";

            usuarios.forEach(usuario => {
                const fila = document.createElement("tr");
                const columnas = [
                    usuario.id,
                    usuario.name,
                    usuario.username,
                    usuario.email,`${usuario.address.street}, ${usuario.address.suite}, ${usuario.address.city}, ${usuario.address.zipcode}, ${usuario.address.geo.lat}, ${usuario.address.geo.lng}`,
                    usuario.phone,
                    usuario.website,`${usuario.company.name} "${usuario.company.catchPhrase}, ${usuario.company.bs}"`
                ];

                columnas.forEach(columna => {
                    const celda = document.createElement("td");
                    celda.textContent = columna;
                    fila.appendChild(celda);
                });

                UsuariosBody.appendChild(fila);
            });
        }
    };

    http.open('GET', url, true);
    http.send();
}

function limpiarDetalles() {
    let UsuariosBody = document.getElementById("UsuariosBody");
    UsuariosBody.innerHTML = ""; 
}

document.getElementById("btnCargar").addEventListener("click", hacerPeticion);
document.getElementById("btnLimpiar").addEventListener("click", limpiarDetalles);
