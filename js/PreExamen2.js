function cargarListaRazas() {
    fetch("https://dog.ceo/api/breeds/list")
        .then(response => response.json())
        .then(data => {
            document.getElementById("raza").innerHTML = "";
            document.getElementById("raza").add(new Option('Selecciona una raza', ''));
            data.message.forEach(raza => {
                let option = new Option(raza, raza);
                document.getElementById("raza").add(option);
            });
        })
        .catch(error => {
            console.error("Ha ocurrido un error: " + error);
        });
}

function verImagen() {
    let raza = document.getElementById("raza").value;

    if (raza) {
        let urlImagen = `https://dog.ceo/api/breed/${raza}/images/random`;

        fetch(urlImagen)
            .then(response => response.json())
            .then(data => {
                document.getElementById("img").src = data.message;
            })
            .catch(error => {
                console.error("Ha ocurrido un error:", error);
            });
    } else {
        alert("Por favor seleccione una raza.");
    }
}

document.addEventListener("DOMContentLoaded", function () {
    document.getElementById("btnCargar").addEventListener("click", function () {
        cargarListaRazas();
    });

    document.getElementById("btnVer").addEventListener("click", function () {
        verImagen();
    });
});
