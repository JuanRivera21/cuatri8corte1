
hacerPeticion=()=>{
    const url = "https://jsonplaceholder.typicode.com/users/"
    const id = document.getElementById("id").value;
    axios.get(url+id)
    //.then(respuesta => respuesta.json())
    .then(data => mostrarDatos(data))
    .catch((reject)=>{
        console.log("Surgió un error "+ reject)
    })
}



mostrarDatos = (response) => {
    console.log(response);
    const data = response.data; // Accede a la propiedad "data" de la respuesta de axios

    const nombre = document.getElementById("nombre");
    const nomU = document.getElementById("nombre_Usuario");
    const email = document.getElementById("email");
    const calle = document.getElementById("calle");
    const numero = document.getElementById("numero");
    const ciudad = document.getElementById("ciudad");

    nombre.value = data.name;
    nomU.value = data.username;
    email.value = data.email;
    calle.value = data.address.street;
    numero.value = data.address.suite;
    ciudad.value = data.address.city;
}


document.getElementById("btnBuscar").addEventListener('click', function(){
    hacerPeticion();
})