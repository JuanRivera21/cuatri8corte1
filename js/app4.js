function mostrarUsuario() {
    const userIdInput = document.getElementById("id");
    const userId = userIdInput.value.trim();

    if (userId === "") {
        alert("Ingrese un ID.");
        return;
    }

    const http = new XMLHttpRequest();
    const url = `https://jsonplaceholder.typicode.com/users/${userId}`;

    http.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {

                const info = document.getElementById("infoUsuario");
                const usuario = JSON.parse(this.responseText);

                info.innerHTML = "";

                Object.entries(usuario).forEach(([clave, valor]) => {
                    const parrafo = document.createElement("p");

                    if (typeof valor === 'object' && clave !== 'geo') {
                        parrafo.textContent = `${clave}:`;

                        const listaAnidada = document.createElement("ul");

                        Object.entries(valor).forEach(([subclave, subvalor]) => {
                            const itemLista = document.createElement("li");

                            if (subclave === 'geo' && clave === 'address') {
                                Object.entries(subvalor).forEach(([geoClave, geoValor]) => {
                                    const geoItemLista = document.createElement("li");
                                    geoItemLista.textContent = `${geoClave}: ${geoValor}`;
                                    listaAnidada.appendChild(geoItemLista);
                                });
                            } else {
                                itemLista.textContent = `${subclave}: ${subvalor}`;
                                listaAnidada.appendChild(itemLista);
                            }
                        });

                        // Agregar la lista al párrafo
                        parrafo.appendChild(listaAnidada);
                    } else {
                        parrafo.textContent = `${clave}: ${valor}`;
                    }

                    info.appendChild(parrafo);
                });
            } else {
                alert("No se encontró el usuario.");
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}



document.getElementById("btnMostrar").addEventListener("click", function(){
    mostrarUsuario();
});

document.getElementById("btnLimpiar").addEventListener("click", function(){
    const info = document.getElementById("infoUsuario");
    info.innerHTML = "";
    document.getElementById("id").value = "";
});
